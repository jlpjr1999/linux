call plug#begin('~/.vim/plugged')


"UI Plugins"
Plug 'scrooloose/nerdtree' "file explorer

Plug 'mbbill/undotree' "Undo tree menu

Plug 'w0rp/ale' "Rsync code linter

Plug 'itchyny/lightline.vim' "Status bar at bottom of screen


call plug#end()
