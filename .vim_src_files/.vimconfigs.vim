""""""""""""""""""""""""""""""""""""""""""
"QoL Changes (adding \"no\" in front of  " 
"		some can turn them off)  "
""""""""""""""""""""""""""""""""""""""""""

"Put numbers on side of vim"
set number "can set relativenumber as well"

set autoread  "combines changes from outside editors into vim "
	      "	 so that it uses the most recent file changes."

set so=4      "sets limit before page scrolls to 4"

set wildmenu "gives a menu that allows you to see the options"
	     "  in the tab auto complete                     "
      
set hlsearch  "highlights search results"
""""""""""""""""""""""""""""""""""""""""""
"Mapping: allows a special character to be"
"  entered so it changes the functions of " 
"  some key presses                       "
""""""""""""""""""""""""""""""""""""""""""

let mapleader="\<space>"

"<cr> means carriage return or the enter key"

"quick save"
map <leader>w :w!<cr>

"autoreload"
map <leader>q :so $MYVIMRC<cr>

"no highlight"
map <leader>h :noh<cr>

"spell checker, press \"z=\" to correct the word, "
"[s or ]s to go to next word                      "
"zg to add to dictionary                          "
map <leader>ss :setlocal spell!<cr>

"split window vertically. to do horizontal, do :split"
map <leader>z :vsplit 

"remap moving between split window keys"
"up"
map <C-j> <C-w>j
"down"
map <C-k> <C-w>k
"left"
map <C-l> <C-w>l
"right"
map <C-h> <C-w>h

""""""""""""""""""""""""""""""""""""""""""
"plugins/filetypes"
""""""""""""""""""""""""""""""""""""""""""

filetype plugin on  "allows plugins to work"
filetype indent on  "automatically indents written code"



""""""""""""""""""""""""""""""""""""""""""
"toggles"
""""""""""""""""""""""""""""""""""""""""""
set pastetoggle=<F1>  "makes it so that the indent plugin doesn't affect"
		      "  pasting format. Binds the option to F1 so I dont"
		      "  need to write \":set paste\" all the time.     "


"""""""""""""""""""""""""""""""""""""""""
"Color schemes"
"""""""""""""""""""""""""""""""""""""""""
colorscheme blackboard


